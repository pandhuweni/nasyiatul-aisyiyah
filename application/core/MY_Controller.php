<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class MY_Controller extends CI_Controller 
 { 
      function __construct()
    {
      parent::__construct();
    }
   //set the class variable.
   var $template  = array();
   var $data      = array();
   //Load layout    
   public function layout() {
     // making temlate and send data to view.
     // making temlate and send data to view.
     $this->template['head']   = $this->load->view('templates/_parts/head_view', $this->data, true);
     $this->template['navigation']   = $this->load->view('templates/_parts/navigation_view', $this->data, true);
     $this->template['content'] = $this->load->view($this->content, $this->data, true);
     $this->template['footer'] = $this->load->view('templates/_parts/footer_view', $this->data, true);
     $this->template['foot'] = $this->load->view('templates/_parts/foot_view', $this->data, true);
     $this->load->view('templates/template_view', $this->template);
   }
}