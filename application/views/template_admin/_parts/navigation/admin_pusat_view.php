  <header class="main-header">
    <!-- Logo -->
    <a href="<?php echo base_url('admin_pusat/dashboard')?>" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>N</b>A</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>Admin</b> Pusat</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="<?php echo base_url('assets/dist/img/user2-160x160.jpg');?>" class="user-image" alt="User Image">
              <span class="hidden-xs">Lorem Ipsum</span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <img src="<?php echo base_url('assets/dist/img/user2-160x160.jpg');?>" class="img-circle" alt="User Image">

                <p>
                  Lorem Ipsum - Ketua Wilayah III
                </p>
              </li>
              </li>
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-left">
                  <a href="#" class="btn btn-default btn-flat">Profil</a>
                </div>
                <div class="pull-right">
                  <a href="#" class="btn btn-default btn-flat">Keluar</a>
                </div>
              </li>
            </ul>
          </li>
        </ul>
      </div>
    </nav>
  </header>

  <!--Sidebar ASIDE-->
  <aside class="main-sidebar">
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="<?php echo base_url('assets/dist/img/user2-160x160.jpg');?>" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>Lorem Ipsum</p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu">
        <li class="header">NAVIGASI UTAMA</li>
        <li class="<?php if($current_page == 'admin_pusat/dashboard') { echo 'active'; } ?>">
          <a href="<?php echo base_url('admin_pusat/dashboard');?>" >
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
            
          </a>
        </li>
        <li class="<?php if($current_page == 'admin_pusat/profil') { echo 'active'; } ?>">
          <a href="<?php echo base_url('admin_pusat/profil');?>" >
            <i class="fa fa-dashboard"></i> <span>Profil</span>
            
          </a>
        </li>

        <li class="<?php if($current_page == 'admin_pusat/gallery') { echo 'active'; } ?>">
          <a href="<?php echo base_url('admin_pusat/gallery');?>" >
            <i class="fa fa-dashboard"></i> <span>Gallery</span>
            
          </a>
        </li>
        <li class="<?php if($current_page == 'admin_pusat/berita'){ echo 'active'; }elseif ($current_page == 'admin_pusat/berita/tambah'){ echo 'active'; }elseif ($current_page == 'admin_pusat/berita/edit'){ echo 'active'; } ?>">
          <a href="<?php echo base_url('admin_pusat/berita');?>">
            <i class="fa fa-newspaper-o"></i> <span>Kelola Berita</span>            
          </a>
        </li>
        
        <li class="<?php if($current_page == 'admin_pusat/approve') { echo 'active'; } ?>">
          <a href="<?php echo base_url('admin_pusat/approve');?>" >
            <i class="fa fa-dashboard"></i> <span>Approve KTNA</span>
            
          </a>
        </li>
        
      </ul>
    </section>
    <!-- /.sidebar ASIDE -->
  </aside>
