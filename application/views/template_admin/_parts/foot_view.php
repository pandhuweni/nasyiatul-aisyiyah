<!-- jQuery 2.2.3 -->
<script src="<?php echo base_url('assets/plugins/jQuery/jquery-2.2.3.min.js'); ?>"></script>
<!-- jQuery UI 1.11.4 -->
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.6 -->
<script src="<?php echo base_url('assets/bootstrap/js/bootstrap.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/datatables/jquery.dataTables.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/datatables/dataTables.bootstrap.min.js'); ?>">
	
</script>
<!-- FastClick -->
<script src="<?php echo base_url('assets/plugins/fastclick/fastclick.js');?>"></script>
<script src="<?php echo base_url('assets/dist/js/app.min.js')?>"></script>


<!-- CK Editor -->
<script type="text/javascript" src="<?php echo base_url('assets/plugins/ckeditor/ckeditor.js'); ?>"></script>
<!-- <script src="https://cdn.ckeditor.com/4.5.7/standard/ckeditor.js"></script> -->
<!-- <script src="//cdn.ckeditor.com/4.6.1/full/ckeditor.js"></script> -->
<!-- Bootstrap WYSIHTML5 -->
<script src="<?php echo base_url('assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js'); ?>"></script>

<script type="text/javascript">
  $(function () {
    $('#berita-table').DataTable({
      "paging": true,
      "lengthChange": true,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "language":{      
        "decimal":        "",
        "emptyTable":     "Tidak ada data di dalam tabel",
        "info":           "Menampilkan _START_ sampai _END_ dari total _TOTAL_ ",
        "infoEmpty":      "Tidak ada berita",
        "infoFiltered":   "(Disaring dari _MAX_ total)",
        "infoPostFix":    "",
        "thousands":      ",",
        "lengthMenu":     "Menampilkan _MENU_ berita",
        "loadingRecords": "Memuat...",
        "processing":     "Memuat...",
        "search":         "Pencatian",
        "zeroRecords":    "Tidak ada data yang cocok",
        "paginate": {
            "first":      "Pertama",
            "last":       "Terakhir",
            "next":       "Selanjutnya",
            "previous":   "Sebelumnya"
        },
        "aria": {
            "sortAscending":  ": activate to sort column ascending",
            "sortDescending": ": activate to sort column descending"
        }
        
      }
    });
  });
  $( ".approval" ).click(function() {
    if($(this).hasClass('btn-inactive')){
      $(this).removeClass("btn-inactive");
       $(this).html("<i class=\"fa fa-check\"></i>&nbsp\; Approve")
    }else{
      $(this).addClass("btn-inactive");
      $(this).html("<i class=\"fa fa-check-circle\"></i>&nbsp\; Approved")
    }
    //$( this ).toggleClass( "btn-inactive" );
  });

  $( ".publish" ).click(function() {
    if($(this).hasClass('btn-inactive')){
      $(this).removeClass("btn-inactive");
       $(this).html("<i class=\"fa fa-check\"></i>&nbsp\; Publish")
    }else{
      $(this).addClass("btn-inactive");
      $(this).html("<i class=\"fa fa-check-circle\"></i>&nbsp\; Published")
    }
    //$( this ).toggleClass( "btn-inactive" );
  });
</script>
<script>
  $(function () {
    // Replace the <textarea id="editor1"> with a CKEditor
    // instance, using default configuration.
    CKEDITOR.replace('edit-berita');

  });
  CKEDITOR.editorConfig = function( config ) {
  // Other configs
  config.filebrowserImageBrowseUrl = '/ckeditor/pictures';
  config.filebrowserImageUploadUrl = '/ckeditor/pictures';

};
</script>