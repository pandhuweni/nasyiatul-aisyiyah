<!DOCTYPE html>
<html>
 <?php if($head) echo $head ;?>
	<body class="hold-transition skin-green fixed sidebar-mini">
		<div class="wrapper">
			 <?php if($navigation) echo $navigation ;?>
			 
			 <?php if($content) echo $content ;?>
			 <?php if($footer) echo $footer; ?>
			 <?php if($foot) echo $foot ;?>
			 
		</div>
	</body>
</html>