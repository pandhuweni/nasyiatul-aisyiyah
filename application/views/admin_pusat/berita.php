
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Berita
      <small>Halaman Untuk Kelola Berita</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="#">Layout</a></li>
      <li class="active">Fixed</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="callout callout-success">
      <h4><i class="fa fa-check"></i> Sukses!</h4>
      <p>Berita baru berhasil ditambahkan!</p>
    </div>
    <div class="callout callout-danger">
      <h4><i class="fa fa-ban"></i> Gagal!</h4>
      <p>Gagal menambah berita baru!</p>
    </div>
    <!-- Default box -->
    
    <div class="box">
      <div class="box-header">
        <h3 class="box-title">Daftar Berita</h3>

        
      </div>
      <!-- /.box-header -->
      <div class="box-body">
        <table id="berita-table" class="table table-bordered table-hover">
          <thead>
          <tr>
            <th>No</th>
            <th>Judul Berita</th>
            <th>Tanggal</th>
            <th>Status</th>
            <th>Action</th>
          </tr>
          </thead>
          <tbody>
            <tr>
              <td>1</td>
              <td>Lorem Ipsum is simply dummy text </td>
              <td>15 Januari 2017</td>
              <td>
                <button class="btn btn-xs btn-success approval">
                  <i class="fa fa-check-circle"></i>
                  &nbsp; Approve
                </button>
              </td>                    
              <td>
                <a class="btn btn-xs btn-info" href="<?php echo base_url('berita/read');?>">
                  <i class="fa fa-info"></i>
                  &nbsp; Detail
                </a>
                <a class="btn btn-xs btn-warning" href="<?php echo base_url('berita/edit');?>">
                  <i class="fa fa-pencil"></i>
                  &nbsp; Edit
                </a>
                <button class="btn btn-xs btn-danger">
                  <i class="fa fa-trash"></i>
                  &nbsp; Hapus
                </button>
              </td>
            </tr>
            <tr>
              <td>2</td>
              <td>Lorem Ipsum is simply dummy text </td>
              <td>13 Januari 2017</td>
              <td>
                <button class="btn btn-xs btn-success approval">
                  <i class="fa fa-check-circle"></i>
                  &nbsp; Approve
                </button>
              </td>                    
              <td>
                <button class="btn btn-xs btn-info">
                  <i class="fa fa-info"></i>
                  &nbsp; Detail
                </button>
                <button class="btn btn-xs btn-warning">
                  <i class="fa fa-pencil"></i>
                  &nbsp; Edit
                </button>
                <button class="btn btn-xs btn-danger">
                  <i class="fa fa-trash"></i>
                  &nbsp; Hapus
                </button>
              </td>
            </tr>
          </tbody>
          <tfoot>
          <tr>
            <th>No</th>
            <th>Judul Berita</th>
            <th>Tanggal</th>
            <th>Status</th>
            <th>Action</th>
          </tr>
          </tfoot>

        </table>
        <a class="btn btn-info pull-right" href="<?php echo base_url('berita/tambah');?>">
          <i class="fa fa-plus"></i>
          &nbsp; Tambah Berita
        </a>
      </div>
    </div>
  </section>

</div>