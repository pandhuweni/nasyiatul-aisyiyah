
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Gallery
      <small>Kumpulan foto - foto kegiatan organisasi</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="#">Layout</a></li>
      <li class="active">Fixed</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <!-- Default box -->
    <div class="box">
      <div class="box-header with-border">
        <h3 class="box-title">Gallery</h3>

        <div class="box-tools pull-right">
          <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
            <i class="fa fa-minus"></i></button>
          <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
            <i class="fa fa-times"></i></button>
        </div>
      </div>
      <div class="box-body">
        <button class="btn btn-info pull-right" data-toggle="modal" data-target="#tambahFotoGalery">
          <i class="fa fa-plus"></i>
          &nbsp; Tambah Foto Gallery
        </button>
      </div>
      <div class="row">
        <div class="col-md-4 thumb">
          <a href="#" class="thumbnail" data-toggle="modal" data-target="#lihatGambar">
            <img src="<?php echo base_url('assets/dist/img/avatar.png'); ?>">
          </a>
          <div class="caption">
            <h4>Judul Foto
            <br>
            <small>Keterangan Foto lorem ipsum</small>
            </h4>
          </div>
        </div>
        <div class="col-md-4 thumb">
          <a href="#" class="thumbnail" data-toggle="modal" data-target="#lihatGambar">
            <img src="<?php echo base_url('assets/dist/img/avatar.png'); ?>">
          </a>
          <div class="caption">
            <h4>Judul Foto
            <br>
            <small>Keterangan Foto lorem ipsum</small>
            </h4>
          </div>
        </div>
        <div class="col-md-4 thumb">
          <a href="#" class="thumbnail" data-toggle="modal" data-target="#lihatGambar">
            <img src="<?php echo base_url('assets/dist/img/avatar.png'); ?>">
          </a>
          <div class="caption">
            <h4>Judul Foto
            <br>
            <small>Keterangan Foto lorem ipsum</small>
            </h4>
          </div>
        </div>
        <div class="col-md-12 thumb">

        <!--Pagination-->
        <nav aria-label="Page navigation" class="pull-right">
          <ul class="pagination">
            <li>
              <a href="#" aria-label="Previous">
                <span aria-hidden="true">&laquo;</span>
              </a>
            </li>
            <li><a href="#">1</a></li>
            <li><a href="#">2</a></li>
            <li><a href="#">3</a></li>
            <li><a href="#">4</a></li>
            <li><a href="#">5</a></li>
            <li>
              <a href="#" aria-label="Next">
                <span aria-hidden="true">&raquo;</span>
              </a>
            </li>
          </ul>
        </nav>
          
        </div>
      </div>
    </div>

  </section>

  <!--Upload-->
  <div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="addMenuTambahan" id="tambahFotoGalery">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Upload Foto di gallery baru</h4>
        </div>
        <div class="modal-body">

          <div class="form-group">
            <label for="exampleInputFile">Pilih Gambar</label>
            <input type="file" id="exampleInputFile">

            <p class="help-block">Format fila gambar yang didukung adalah .jpg, .png, dan .gif</p>
          </div>
          <div class="form-group">
            <label >Judul Foto</label>
            <input type="text" class="form-control" placeholder="Judul Foto">
          </div>

          <div class="form-group">
            <label >Keterangan Foto</label>
            <textarea placeholder="Keterangan" class="form-control" rows="3"></textarea>
          </div>
        </div>

        <div class="modal-footer">
          <button type="button" class="btn btn-primary">Tambah</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
        </div>
      </div>
    </div>
  </div>

  !--Upload-->
  <div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="addMenuTambahan" id="lihatGambar">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Upload Foto di gallery baru</h4>
        </div>
        <div class="modal-body">

          <div class="col-md-12">
          <a href="#" class="thumbnail" data-toggle="modal" data-target="#lihatGambar">
            <img src="<?php echo base_url('assets/dist/img/avatar.png'); ?>">
          </a>
        </div>
          <div class="form-group">
            <label >Judul Foto</label>
            <p>
              Judul Foto Lorem Ipsum
            </p>
          </div>

          <div class="form-group">
            <label >Keterangan Foto</label>
            <p>
              Keternagan foto lorem ipsum dolor sit amet
            </p>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>