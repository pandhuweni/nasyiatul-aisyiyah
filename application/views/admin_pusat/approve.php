
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Approve KTNA
      <small>Halaman Untuk Menyetujui Pendaftaran KTNA</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="#">Layout</a></li>
      <li class="active">Fixed</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <!-- Default box -->
    
    <div class="box">
      <div class="box-header">
        <h3 class="box-title">Data Pendaftaran</h3>
      </div>
      <!-- /.box-header -->
      <div class="box-body">
        <table id="berita-table" class="table table-bordered table-hover">
          <thead>
          <tr>
            <th>No</th>
            <th>Judul Berita</th>
            <th>Tanggal</th>
            <th>Status</th>
            <th>Action</th>
          </tr>
          </thead>
          <tbody>
            <tr>
              <td>1</td>
              <td>Lorem Ipsum is simply dummy text </td>
              <td>15 Januari 2017</td>
              <td>
                <button class="btn btn-xs btn-success approval">
                  <i class="fa fa-check-circle"></i>
                  &nbsp; Approve
                </button>
              </td>                    
              <td>
                <button class="btn btn-xs btn-info" data-toggle="modal" data-target="#detailDaftar">
                  <i class="fa fa-info"></i>
                  &nbsp; Detail
                </button>
                <button class="btn btn-xs btn-warning" data-toggle="modal" data-target="#editDaftar">
                  <i class="fa fa-pencil"></i>
                  &nbsp; Edit
                </button>
                <button class="btn btn-xs btn-danger" data-toggle="modal" 
                data-target="#deleteDaftar">
                  <i class="fa fa-trash"></i>
                  &nbsp; Hapus
                </button>
              </td>
            </tr>
            <tr>
              <td>2</td>
              <td>Lorem Ipsum is simply dummy text </td>
              <td>13 Januari 2017</td>
              <td>
                <button class="btn btn-xs btn-success approval">
                  <i class="fa fa-check-circle"></i>
                  &nbsp;
                  Approve
                </button>
              </td>                    
              <td>
                <button class="btn btn-xs btn-info" data-toggle="modal" data-target="#detailDaftar">
                  <i class="fa fa-info"></i>
                  &nbsp; Detail
                </button>
                <button class="btn btn-xs btn-warning" data-toggle="modal" data-target="#editDaftar">
                  <i class="fa fa-pencil"></i>
                  &nbsp; Edit
                </button>
                <button class="btn btn-xs btn-danger" data-toggle="modal" 
                data-target="#deleteDaftar">
                  <i class="fa fa-trash"></i>
                  &nbsp; Hapus
                </button>
              </td>
            </tr>
          </tbody>
          <tfoot>
          <tr>
            <th>No</th>
            <th>Judul Berita</th>
            <th>Tanggal</th>
            <th>Status</th>
            <th>Action</th>
          </tr>
          </tfoot>

        </table>
      </div>
    </div>
  </section>

  <!--mocal detail for detail-->
  <div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" id="detailDaftar">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Data Pendaftar KTNA</h4>
        </div>
        <div class="modal-body">
          <div class="row">
              <div class="col-md-4">
                <div href="#" class="thumbnail" >
                  <img src="<?php echo base_url('assets/dist/img/3x4.JPG')?>" alt="Nama File">
                </div>  
              </div>
              <div class="col-md-8">
                <h4>Lorem Ipsum</h4>
                <table>
                  <tr>
                    <td>Nama</td>
                    <td>Lorem Ipsum</td>
                  </tr>
                  <tr>
                    <td>Alamat</td>
                    <td>Jalan Kaliurang no 4 </td>
                  </tr>
                  <tr>
                    <td>Tanggal Lahir</td>
                    <td>23 Januari 1980</td>
                  </tr>
                  <tr>
                    <td>Wilayah</td>
                    <td>Kawil III / Kebumen</td>
                  </tr>
                </table>
              </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-success">Approve</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
    <!-- /.modal-dialog for detail-->


  <!--mocal detail for edit-->
  <div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="editLabel" id="editDaftar">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Edit Data</h4>
        </div>
        <div class="modal-body">
          <div class="row">
              <div class="col-md-4">
                <div href="#" class="thumbnail" >
                  <img src="<?php echo base_url('assets/dist/img/3x4.JPG')?>" alt="Nama File">
                </div>  
              </div>
              <div class="col-md-8">
                <h4>Lorem Ipsum</h4>
                <table class="table">
                  <tr>
                    <td>Nama</td>
                    <td >
                      <input type="text" name="" class="form-control">
                    </td>
                  </tr>
                  <tr>
                    <td>Alamat</td>
                    <td >
                      <input type="text" name="" class="form-control">
                    </td>
                  </tr>
                  <tr>
                    <td>Tanggal Lahir</td>
                    <td >
                      <input type="text" name="" class="form-control">
                    </td>
                  </tr>
                  <tr>
                    <td>Wilayah</td>
                    <td >
                      <input type="text" name="" class="form-control">
                    </td>
                  </tr>
                </table>
              </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-success">Approve</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
  <!-- /.modal-dialog for edit-->

  <!--mocal detail for delete-->
  <div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="deleteLabel" id="deleteDaftar">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Yakin ingin menghapus data ini?</h4>
        </div>
        <div class="modal-body">
          <div class="row">
              <div class="col-md-4">
                <div href="#" class="thumbnail" >
                  <img src="<?php echo base_url('assets/dist/img/3x4.JPG')?>" alt="Nama File">
                </div>  
              </div>
              <div class="col-md-8">
                <h4>Lorem Ipsum</h4>
                <table>
                  <tr>
                    <td>Nama</td>
                    <td>Lorem Ipsum</td>
                  </tr>
                  <tr>
                    <td>Alamat</td>
                    <td>Jalan Kaliurang no 4 </td>
                  </tr>
                  <tr>
                    <td>Tanggal Lahir</td>
                    <td>23 Januari 1980</td>
                  </tr>
                  <tr>
                    <td>Wilayah</td>
                    <td>Kawil III / Kebumen</td>
                  </tr>
                </table>
              </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-success">Approve</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
  <!-- /.modal-dialog for delete-->
</div>