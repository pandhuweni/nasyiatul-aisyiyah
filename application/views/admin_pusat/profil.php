
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Profil
      <small>Halaman untuk mengatur profil organisasi</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="#">Layout</a></li>
      <li class="active">Fixed</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <!-- Default box -->
    <div class="box">
      <div class="box-header with-border">
        <h3 class="box-title">Daftar Halaman Profil</h3>

        <div class="box-tools pull-right">
          <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
            <i class="fa fa-minus"></i></button>
          <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
            <i class="fa fa-times"></i></button>
        </div>
      </div>

      <div class="box-body">
          <div class="callout callout-info">
            <h4>Hint!</h4>
            <p>Daftar halaman di bawah akan muncul di bagian navigasi Profil</p>
          </div>
          <h4>Level 1 <br>
            <small>Muncul di tingkat pertama turunan Profil</small>
          </h4>

           <button class="btn btn-info pull-right" data-toggle="modal" 
                data-target="#tambahMenuUtama">
            <i class="fa fa-plus"></i>
            &nbsp; Tambah Menu Utama
          </button>
          <table class="table table-striped">
            <thead>
              <tr>
                <th>No</th>
                <th>Nama Halaman</th>
                <th>Status</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>1</td>
                <td>Profil</td>
                <td>
                  <button class="btn btn-xs btn-success publish">
                    <i class="fa fa-check"></i>
                    &nbsp;
                    Publish
                  </button>
                </td>              
                <td>
                <a  href="<?php echo base_url('admin_pusat/profil/baca') ?>" class="btn btn-xs btn-info">
                  <i class="fa fa-info"></i>
                  &nbsp; Detail
                </a>
                <a href="<?php echo base_url('admin_pusat/profil/edit') ?>" class="btn btn-xs btn-warning">
                  <i class="fa fa-pencil"></i>
                  &nbsp; Edit
                </a>
                <button class="btn btn-xs btn-danger" data-toggle="modal" 
                data-target="#deleteMenu">
                  <i class="fa fa-trash"></i>
                  &nbsp; Hapus
                </button>
                </td>
              </tr>
            </tbody>
          </table>

          <hr>

          <h4>Level 2 <br>
            <small>Muncul di tingkat kedua turunan Profil</small>
          </h4>

           <button class="btn btn-info pull-right" data-toggle="modal" 
                data-target="#addMenuTambahan">
            <i class="fa fa-plus"></i>
            &nbsp; Tambah Menu Turunan
          </button>
          <table class="table table-striped">
            <thead>
              <tr>
                <th>No</th>
                <th>Nama Halaman</th>                
                <th>Parent</th>
                <th>Status</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>1</td>
                <td>Profil Divisi Humas</td>
                <td>Profil</td>
                <td>
                  <button class="btn btn-xs btn-success publish">
                    <i class="fa fa-check"></i>
                    &nbsp;
                    Publish
                  </button>
                </td>              
                <td>
                <a  href="<?php echo base_url('admin_pusat/profil/baca') ?>" class="btn btn-xs btn-info">
                  <i class="fa fa-info"></i>
                  &nbsp; Detail
                </a>
                <a href="<?php echo base_url('admin_pusat/profil/edit') ?>" class="btn btn-xs btn-warning">
                  <i class="fa fa-pencil"></i>
                  &nbsp; Edit
                </a>
                <button class="btn btn-xs btn-danger" data-toggle="modal" 
                data-target="#deleteMenu">
                  <i class="fa fa-trash"></i>
                  &nbsp; Hapus
                </button>
                </td>
              </tr>
            </tbody>
          </table>
      </div>
      <!-- /.box-body -->
    </div>

  </section>

    <!--Modal add Menu Utama-->
<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="addMenuUtama" id="tambahMenuUtama">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Tambah Menu Utama</h4>
        </div>
        <div class="modal-body">
          <div class="form-group">
            <label >Menu Baru</label>
            <input type="text" class="form-control" placeholder="Masukkan nama menu baru">
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-primary">Tambah</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
        </div>
      </div>
    </div>
  </div>

    <!--Modal add Menu Tambahan-->

<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="addMenuTambahan" id="addMenuTambahan">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Tambah Menu Turunan</h4>
        </div>
        <div class="modal-body">

          <div class="form-group">
            <label >Parent</label>
            <select class="form-control">
              <option selected="selected" disabled="disabled">- Pilih Parent -</option>
              <option>Profil</option>
              <option>Visi Misi </option>
            </select>
          </div>
          <div class="form-group">
            <label >Menu Baru</label>
            <input type="text" class="form-control" placeholder="Masukkan nama menu baru">
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-primary">Tambah</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
        </div>
      </div>
    </div>
  </div>

   <!--Modal delete-->
   <div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="deleteLabel" id="deleteMenu">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Yakin ingin menghapus menu/halaman profil?</h4>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-primary">Ya</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
        </div>
      </div>
    </div>
  </div>
</div>