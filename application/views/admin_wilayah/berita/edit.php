
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Berita
      <small>Halaman Untuk Kelola Berita</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="#">Layout</a></li>
      <li class="active">Fixed</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <!-- Default box -->
    <div class="box">
      <div class="box-header">
        <h3 class="box-title">Edit Berita</h3>
        <button class="btn btn-primary pull-right">
          <i class="fa fa-check"></i>
          &nbsp;  Finish Edit
        </button>

        <button class="btn btn-danger pull-right" style="margin-left: 5px; margin-right: 5px">
          <i class="fa fa-ban"></i>
          &nbsp;  Delete
        </button>
      </div>
      <!-- /.box-header -->
      <div class="box-body baca-berita">
        <div class="row">
          <div class="col-md-12 pad">
            <div class="form-group">
              <label>Judul Berita</label>
              <input type="text" class="form-control" value="Lorem ipsum dolor sit amet" id="inputJudulBerita">
            </div>
            <form>
              <textarea id="edit-berita" rows="20" cols="80">
              <p>
                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
              </p>
              <div class="thumbnail">
                <img src="<?php echo base_url('assets/dist/img/berita/post-1.jpg') ?>">
                <center>
                  <small>Picture 1.1 This is caption of the image!</small>
                </center>
              </div>
              <p>          
                Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32. 
              </p>
              <blockquote>
                The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested.
              </blockquote>
              <p>         
                The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from "de Finibus Bonorum et Malorum" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.
              </p>
              </textarea>
            </form>          
            <!-- <hr>
            <center>
              <h5> 
                Preview              
              </h5>              
            </center>
            <hr> -->
            </div>
            <div class="col-md-12">
            <br>
            <button class="btn btn-primary pull-right">
              <i class="fa fa-check"></i>
              &nbsp;  Finish Edit
            </button>
              
            </div>
          <!-- <div class="col-md-4 pull-right">
            <div class="panel panel-default">
              <div class="panel-heading judul-berita" >
                Detail Berita
              </div>
              <div class="panel-body">
                <center>                    
                  <img src="<?php echo base_url('assets/dist/img/user2-160x160.jpg');?>" class="img-circle" alt="Post">

                  <h4 class="keterangan-user">
                    Lorem Ipsum (Berita Maker)
                  </h4>
                </center>
                <hr>
                <p>
                  <strong>
                    Created on
                  </strong>
                  <br>
                  12 Januari 2017
                </p>

                <p>
                  <strong>
                    Status
                  </strong>
                  <br>
                  Approved
                </p>
              </div>
              
            </div>
          </div> -->
          <!--<div class="col-md-8">
            <h4 id="judul-berita">Lorem ipsum dolor sit amet</h4>
            <div class="preview-berita" id="detail-berita">
              <p>
              Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
              tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
              quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
              consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
              cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
              proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
            </p>
            <div class="thumbnail">
              <img src="<?php echo base_url('assets/dist/img/berita/post-1.jpg') ?>">
              <center>
                <small>Picture 1.1 This is caption of the image!</small>
              </center>
            </div>
            <p>          
              Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32. 
            </p>
            <blockquote>
              The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested.
            </blockquote>
            <p>         
              The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from "de Finibus Bonorum et Malorum" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.
            </p>
            </div>
            
          </div> -->
        </div>
        
      </div>
    </div>
  </section>

</div>