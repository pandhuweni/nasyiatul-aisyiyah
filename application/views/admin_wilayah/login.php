<div class="login-box">
  <div class="login-logo">
  	<center>
  		<img src="<?php echo base_url('assets/dist/img/logo.png') ?>" class="img-responsive img-circle img-admin">	
  	</center>
    
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body">
    <p class="login-box-msg">Silakan login untuk masuk ke halaman Admin</p>	

    <form action="<?php echo base_url('admin_wilayah/dashboard') ?>" method="post">
      <div class="form-group has-feedback">
        <input type="email" class="form-control" placeholder="Email">
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="password" class="form-control" placeholder="Password">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
      <div class="row">
        <div class="col-xs-8">
          <div class="checkbox icheck">
            <label>
              <input type="checkbox" class="icheckbox_square-blue">&nbsp; Ingatkan Saya
            </label>
          </div>
        </div>
        <!-- /.col -->
        <div class="col-xs-4">
          <button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
        </div>
        <!-- /.col -->
      </div>


	    <a href="<?php echo base_url(); ?>" class="text-center">
	    	Lupa Password?
	    </a>
    </form>



  </div>

    <a href="<?php echo base_url(); ?>" class="text-center">
    	<i class="fa fa-long-arrow-left" style="margin-top: 20px"></i>
    	&nbsp;
    	Kembali ke website
    </a>
  <!-- /.login-box-body -->
</div>