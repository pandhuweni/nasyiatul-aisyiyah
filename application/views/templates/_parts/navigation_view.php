<div class="wrapper hold-transition skin-blue layout-top-nav">
  <header class="main-header">
    <nav class="navbar navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <a href="<?php echo base_url('welcome') ?>" class="navbar-brand"><b>Nasyatul</b> Aisyiyah</a>
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
            <i class="fa fa-bars"></i>
          </button>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
       
        <!-- Navbar Right Menu -->
        <div class="navbar-custom-menu">
          <ul class="nav navbar-nav">

            <!-- Tasks Menu -->
            <li>
              <!-- Menu Toggle Button -->
              <a href="<?php echo base_url('admin_wilayah/login');?>">
                <i class="fa fa-user-secret"></i>
                &nbsp; Login Wilayah
              </a>
            </li>

            <li>
              <!-- Menu Toggle Button -->
              <a href="<?php echo base_url('admin_daerah/login');?>">
                <i class="fa fa-user-secret"></i>
                &nbsp; Login Daerah
              </a>
            </li>

            <li>
              <!-- Menu Toggle Button -->
              <a href="<?php echo base_url('admin_pusat/login');?>">
                <i class="fa fa-user-secret"></i>
                &nbsp; Login Pusat
              </a>
            </li>
            <!-- User Account Menu -->
            
          </ul>
        </div>
        <!-- /.navbar-custom-menu -->
      </div>
      <!-- /.container-fluid -->
    </nav>
  </header>  
</div>
