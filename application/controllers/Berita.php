<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

include_once(APPPATH.'core/Admin_Controller.php');

class Berita extends Admin_Controller {
	function __construct()
  {
    parent::__construct();
    $this->load->model('MAdminW');
  }

  public function index() {
    $this->data['current_page'] = $this->uri->uri_string();
    $this->data['dataBerita'] = $this->MAdminW->getDataBerita();
    $this->content = 'adminW/berita'; 
    // passing middle to function. change this for different views.
    $this->data['page_title'] = 'Berita | Halaman Admin Nasyiatul Aisyiyah';
    $this->layout();

  }

  public function read(){
    $this->data['current_page'] = $this->uri->uri_string();
    $this->content = 'adminW/berita/read'; 
    // passing middle to function. change this for different views.
    $this->data['page_title'] = 'Baca Berita | Halaman Admin Nasyiatul Aisyiyah';
    $this->layout();
  }

  public function edit(){
    $this->data['current_page'] = $this->uri->uri_string();
    $this->content = 'adminW/berita/edit'; 
    // passing middle to function. change this for different views.
    $this->data['page_title'] = 'Baca Berita | Halaman Admin Nasyiatul Aisyiyah';
    $this->layout();
  }

  public function tambah(){
    $this->data['current_page'] = $this->uri->uri_string();
    $this->content = 'adminW/berita/tambah'; 
    // passing middle to function. change this for different views.
    $this->data['page_title'] = 'Baca Berita | Halaman Admin Nasyiatul Aisyiyah';
    $this->layout();
  }

  public function submitBerita(){
    $judul = $this->input->post('judulBerita');
    $content = $this->input->post('contentBerita');
    
  }
}