<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

include_once(APPPATH.'core/Base_Controller.php');
class Login extends Base_Controller {
  public function index() {
    $this->content = 'login'; 
    // passing middle to function. change this for different views.
    $this->data['page_title'] = 'Login ke Halaman Admin Nasyiatul Aisyiyah';
    $this->layout();
  }
  
}