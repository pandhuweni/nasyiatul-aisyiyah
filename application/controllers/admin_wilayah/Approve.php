<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

include_once(APPPATH.'core/Admin_Controller.php');

class Approve extends Admin_Controller {
    function __construct()
  {
    parent::__construct();
  }

  public function index() {
    $this->data['current_page'] = $this->uri->uri_string();
    $this->navigation = 'template_admin/_parts/navigation/admin_wilayah_view'; 
    $this->content = 'admin_wilayah/approve'; 
    // passing middle to function. change this for different views.
    $this->data['page_title'] = 'Approve KTNA | Halaman Admin Nasyiatul Aisyiyah';
    $this->layout();
  }


}