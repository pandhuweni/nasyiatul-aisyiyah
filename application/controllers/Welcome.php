<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Welcome extends MY_Controller {
  public function index() {
    $this->content = 'home'; 
    // passing middle to function. change this for different views.
    $this->data['page_title'] = 'Nasyiatul Aisyiyah';
    $this->layout();
  }
  
}