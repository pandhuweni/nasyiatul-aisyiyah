<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

include_once(APPPATH.'core/Admin_Controller.php');

class Berita extends Admin_Controller {
  function __construct()
  {
    parent::__construct();
    $this->load->model('MAdminW');
  }

  public function index() {
    $this->data['current_page'] = $this->uri->uri_string();

    $this->navigation = 'template_admin/_parts/navigation/admin_pusat_view'; 
    $this->data['dataBerita'] = $this->MAdminW->getDataBerita();
    $this->content = 'admin_pusat/berita'; 
    // passing middle to function. change this for different views.
    $this->data['page_title'] = 'Berita | Halaman Admin Nasyiatul Aisyiyah';
    $this->layout();

  }

  public function baca(){
    $this->data['current_page'] = $this->uri->uri_string();

    $this->navigation = 'template_admin/_parts/navigation/admin_pusat_view'; 
    $this->content = 'admin_pusat/berita/baca'; 
    // passing middle to function. change this for different views.
    $this->data['page_title'] = 'Baca Berita | Halaman Admin Nasyiatul Aisyiyah';
    $this->layout();
  }

  public function edit(){
    $this->data['current_page'] = $this->uri->uri_string();

    $this->navigation = 'template_admin/_parts/navigation/admin_pusat_view'; 
    $this->content = 'admin_pusat/berita/edit'; 
    // passing middle to function. change this for different views.
    $this->data['page_title'] = 'Edit Berita | Halaman Admin Nasyiatul Aisyiyah';
    $this->layout();
  }

  public function tambah(){
    $this->data['current_page'] = $this->uri->uri_string();

    $this->navigation = 'template_admin/_parts/navigation/admin_pusat_view'; 
    $this->content = 'admin_pusat/berita/tambah'; 
    // passing middle to function. change this for different views.
    $this->data['page_title'] = 'Edit Berita | Halaman Admin Nasyiatul Aisyiyah';
    $this->layout();
  }

  public function submitBerita(){
    $judul = $this->input->post('judulBerita');
    $content = $this->input->post('contentBerita');
    
  }
}