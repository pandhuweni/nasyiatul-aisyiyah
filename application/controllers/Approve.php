<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

include_once(APPPATH.'core/Admin_Controller.php');

class Approve extends Admin_Controller {
    function __construct()
  {
    parent::__construct();
  }

  public function index() {
    $this->data['current_page'] = $this->uri->uri_string();
    $this->content = 'adminW/approve_ktna'; 
    // passing middle to function. change this for different views.
    $this->data['page_title'] = 'Approve KTNA | Halaman Admin Nasyiatul Aisyiyah';
    $this->layout();
  }


}